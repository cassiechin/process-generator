/*
 * Cassie Chin (cassiechin9793@gmail.com) 
 * Process Table Generator
 */
#include "pcb.h"
#include "processTable.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CYCLES 11000
#define MIN_CYCLES 1000
#define MEAN_CYCLES 6000
#define MAX_MEM_KB 100
#define MIN_MEM_KB 1
#define MEAN_MEM_KB 20
#define BYTE_IN_KB 1000
#define MAX_MEM_B (MAX_MEM_KB * BYTE_IN_KB)
#define MIN_MEM_B (MIN_MEM_KB * BYTE_IN_KB)
#define MEAN_MEM_B (MEAN_MEM_KB * BYTE_IN_KB)
#define MEM_MODE 1 // 0 will use KB, 1 will use B

/**
 * Node that stores a process and the next process 
 */
typedef struct ProcessNode ProcessNode;
struct ProcessNode {
	PCB *process;
	ProcessNode *next;
};

/**
 * Table that stores a linked list of process nodes and statistics
 */
struct ProcessTable {
	int numProcesses; // number of nodes in the linked list
	int totalCycles; // use to compute average cycles
	int totalMemory; // use to compute average memory
	ProcessNode *head; // process with the LOWEST pid
	ProcessNode *tail; // process with the HIGHEST pid
};

/**
 * Generate a number between 1,000 and 11,000 with a mean of 6,000
 * @return The random number generated
 */
int generateNumCycles () {	
	return (rand() % (MAX_CYCLES - MIN_CYCLES)) + MIN_CYCLES;
}

/**
 * Generate a number between 1KB and 100KB with a mean of 20KB
 * 
 * 1) Determine whether to generate a number OVER the mean or LOWER than
 *    the mean.
 * 
 *    This is done by generating a random number between the lowerbound
 *    and the upperbound. If the number generated is OVER the mean, then
 * 	  return a random number that is UNDER the mean. If the number
 *    generated is UNDER the mean, then generate a number that is OVER
 *    the mean.
 * 	  
 *    For example if you want a mean of 20 in between 1 and 100, then
 *    you need to average in more numbers under 20. Since you have a
 *    greater probablity of generating a number between 20 and 100, you
 *    use that as a measure for deciding to generate a number under 20.
 * 
 * 2) Generate a random number in that range
 * 
 * A MEM_MODE of 0 will return the number in KB
 * A MEM_MODE of 1 will return the number in B
 * @return The random number generated
 */
int generateMemSize () {
	int upperbound;
	int lowerbound;
	int mean;
	
	switch (MEM_MODE) {
		case 0: 
			upperbound = MAX_MEM_KB;
			lowerbound = MIN_MEM_KB;
			mean = MEAN_MEM_KB;
			break;
		case 1: 
			upperbound = MAX_MEM_B;
			lowerbound = MIN_MEM_B;
			mean = MEAN_MEM_B;
			break;
		default: return 0;
	}
	
	// Get a number under the mean
	if ((rand() % (upperbound - lowerbound)) > (mean - lowerbound)) 
		return (rand() % (mean - lowerbound)) + lowerbound;
	// Get a number over the mean
	else 
		return (rand() % (upperbound - mean)) + mean;
}

/**
 * Generates process AND adds it to the process table
 * @param pt The process table to edit
 * @return The process id
 * @return -1 if there was an error
 */
int processTable_generateNewProcess (ProcessTable *pt) {
	if (!pt) return -1;
	
	// Create a new node
	ProcessNode *newNode = (ProcessNode *) calloc (1, sizeof(ProcessNode));
	if (!newNode) return -1;
		
	// There is NOTHING in the process table
	if (pt->head == NULL && pt->tail == NULL) {
		// Create new process
		PCB *newPCB = new_PCB (1, generateNumCycles(), generateMemSize());
		if (!newPCB) return -1;
		
		// Set the values in the process node
		newNode->process = newPCB;
		//newNode->prev = NULL;
		newNode->next = NULL;
				
		// Add the new node to the process table
		pt->head = newNode;
		pt->tail = newNode;
		pt->totalCycles += pcb_getNumCycles(newPCB);
		pt->totalMemory += pcb_getMemSize(newPCB);
		
		// Return the new process id
		return pcb_getPID(newPCB);
	}
	// There is something in the process table
	else if (pt->tail) {
		// Create new process		
		PCB *newPCB = new_PCB (pcb_getPID(pt->tail->process)+1, generateNumCycles(), generateMemSize());
		if (!newPCB) return -1;
	
		// Set the values in the process node
		newNode->process = newPCB;
		//newNode->prev = pt->tail;
		newNode->next = NULL;
		
		// Add the new node to the process table
		pt->tail->next = newNode;
		pt->tail = newNode;
		pt->totalCycles += pcb_getNumCycles(newPCB);
		pt->totalMemory += pcb_getMemSize(newPCB);
		
		// Return the new process
		return pcb_getPID(newPCB);
	}
	else {
		// ERROR, this means there is a head and NO tail
		return -1;
	}
	
	return -1;
}

/**
 * Creates a process table with an initial number of processes.
 * @param numProcess How many processes to put in the table
 * @return The process table created
 */
ProcessTable *new_ProcessTable (int numProcesses) {
	ProcessTable *self = (ProcessTable *) calloc (1, sizeof(ProcessTable));

	// MAKE SURE THIS IS ONLY CALLED ONCE IN THE ENTIRE PROGRAM
	srand(time(NULL));
	
	int i;
	for (i=0; i<numProcesses; i++) {
		if (processTable_generateNewProcess(self)) {
			self->numProcesses++;
		}
		else {
			printf("Error creating process %d\n", i);
		}
	}
	
	return self;
}

/**
 * Deletes a process table
 * @paream pt The process table to delete
 */
void delete_ProcessTable (ProcessTable *pt) {
	if (pt) free(pt);
	return;
}

/**
 * Searches through the process table for a process.
 * Returns the process if it was successfully found, returns NULL if
 * the process was not found.
 */
/*
PCB *processTable_getProcessByPID (ProcessTable *pt, int pid) {
	if (!pt) return NULL;
	ProcessNode *p;
	for (p=pt->head; p != NULL; p=p->next) {
		if (pcb_getPID(p->process) == pid) return p->process;
		
		// Process numbers always go UP, so if we're at a node in the 
		// table that has a higher pid, then the process is not found
		if (pcb_getPID(p->process) > pid) return NULL;
	}
	return NULL;
}
*/

/**
 * Deletes a process in the table
 * @param pt The process table to edit
 * @param pid The process id of the process to delete
 * @return 0 Indicates Error
 * @return 1 The process was found and removed successfully
 */
int processTable_removeProcessByPID (ProcessTable *pt, int pid) {
	if (!pt) return 0;
	
	// pid is the HEAD process
	if (pcb_getPID(pt->head->process) == pid) {
		PCB *pcbToDelete = pt->head->process;		
		ProcessNode *nodeToDelete = pt->head;
		
		pt->head = nodeToDelete->next;
		if (pt->head == NULL) pt->tail = NULL; // There was only one process
		pt->numProcesses -= 1;
		pt->totalCycles -= pcb_getNumCycles(pcbToDelete);
		pt->totalMemory -= pcb_getMemSize(pcbToDelete);
		
		delete_PCB (pcbToDelete);
		free(nodeToDelete);
		pcbToDelete = NULL;
		nodeToDelete = NULL;
		
		// Success
		return 1;
	}
	// pid is NOT the head process
	else {
		ProcessNode *p;
		ProcessNode *pNext;
		for (p=pt->head; p != NULL; p=p->next) {
			// Check the NEXT node (not the current node)
			pNext = p->next;
			// The NEXT node is the correct process to delete
			if (pcb_getPID(pNext->process) == pid) {
				PCB *pcbToDelete = pNext->process;				
				p->next = pNext->next; // point the current Node over pNext
				
				// Update Table
				if (p->next == NULL) pt->tail = p; // This was the last process				
				pt->numProcesses -= 1;
				pt->totalCycles -= pcb_getNumCycles(pcbToDelete);
				pt->totalMemory -= pcb_getMemSize(pcbToDelete);
				
				delete_PCB (pcbToDelete); pcbToDelete = NULL;
				return 1;
			}	
			// The CURRENT node has a process ID that is larger than the
			// one to search for. This means we passed over the pid
			else if (pcb_getPID(p->process) > pid) {
				return 0;
			}
		}
	}
	
	return 0;
}

/**
 * Print process table number of processes, average cycles, and average
 * memory
 * @param pt Process Table to print out
 */
void processTable_printStatistics (ProcessTable *pt) {
	if (!pt) return;
	printf("numProcesses=%d, averageCycles=%d, averageMem=%d\n",
		pt->numProcesses,
		pt->totalCycles/pt->numProcesses,
		pt->totalMemory/pt->numProcesses);
}

/**
 * Print process table processes
 * @param pt Process Table to print out
 */
void processTable_printProcesses (ProcessTable *pt) {
	if (!pt) return;
	ProcessNode *p;
	for (p=pt->head; p != NULL; p=p->next) {
		pcb_print(p->process);
	}
}

/**
 * Print process table statistics and processes
 * @param pt Process Table to print out
 */
void processTable_print (ProcessTable *pt) {
	if (!pt) return;		
	processTable_printStatistics(pt);
	processTable_printProcesses(pt);
}

void processTable_printStatisticsCSV (ProcessTable *pt) {
	if (!pt) return;
	printf("%d, %d, %d\n", pt->numProcesses, pt->totalCycles/pt->numProcesses, pt->totalMemory/pt->numProcesses);
}

void processTable_printProcessesCSV (ProcessTable *pt) {
	if (!pt) return;
	ProcessNode *p;
	for (p=pt->head; p != NULL; p=p->next) {
		pcb_printCSV(p->process);
	}
}

void processTable_printCSV (ProcessTable *pt) {
	if (!pt) return;
	processTable_printStatisticsCSV(pt);
	processTable_printProcessesCSV(pt);
}
