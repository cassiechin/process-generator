/*
 * Cassie Chin (cassiechin9793@gmail.com) 
 * Process Table Generator
 */
#include "pcb.h"
#include <stdlib.h>
#include <stdio.h>

/**
 * Structure that stores a single process
 */
struct PCB {
	int pid; // Unique process id 
	int numCycles; // number of CPU cycles required to complete the process
	int memSize; // Size of memory footprint
};

/**
 * Creates a pcb structure
 * @param pid The process id
 * @param numCycles The number of cycles
 * @param memSize The size of the memory
 * @return The object created
 */
PCB *new_PCB (int pid, int numCycles, int memSize) {
	PCB *self = (PCB *) calloc (1, sizeof(PCB));
	self->pid = pid;
	self->numCycles = numCycles;
	self->memSize = memSize;	
	return self;
}

/**
 * Deletes a pcb structure
 * @param p The structure to delete
 */
void delete_PCB (PCB *p) {
	if (p) free(p);
	return;
}

/**
 * Returns the pid of a PCB object
 */
int pcb_getPID (PCB *p) {
	if (!p) return -1; 
	return p->pid; 
}

/**
 * Returns the number of cycles of a PCB object
 */
int pcb_getNumCycles (PCB *p) {	
	if (!p) return 0;
	return p->numCycles; 
}

/**
 * Returns the memory footprint size of a PCB object
 */
int pcb_getMemSize (PCB *p) { 
	if (!p) return 0;
	return p->memSize; 
}

/**
 * Print out PCB structure
 * @param p The PCB to print out
 */
void pcb_print (PCB *p) {
	printf("pid=%d, numCycles=%d, memSize(KB)=%d\n", p->pid, p->numCycles, p->memSize);
}

/**
 * Print out PCB structure in CSV format
 * @param p The PCB to print out
 */
void pcb_printCSV (PCB *p) {
	printf("%d, %d, %d\n", p->pid, p->numCycles, p->memSize);
}
