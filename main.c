#include "processTable.h"

int main () {
	// Create process table
	ProcessTable *pt1 = new_ProcessTable(100);

	// Add process to the process table	
	//	int newProcess = processTable_generateNewProcess (pt1);

	// Remove process from the process table
	//	processTable_removeProcessByPID (pt1, 7);

	// Print process table
	//	processTable_print(pt1);
	processTable_printStatistics(pt1);
	processTable_printProcessesCSV(pt1);

	// Delete process table
	delete_ProcessTable(pt1);
	return 0;
}
